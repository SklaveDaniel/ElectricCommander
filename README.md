# Electric Commander

Android app to control estim devices via stereo audio.
Tested with the Estim 2b power box but should work with other devices with audio input.

## Features

- Continous wave generation (adjust frequency and volume for each channel)
- Sound triggered pulses (adjust trigger limit, smoothing, pulse duration and minimum pauses)

## Download
The current binary can be downloaded at:

[electriccommander-release.apk](https://gitlab.com/SklaveDaniel/ElectricCommander/uploads/cf545c7c0791a5ca236fda79e19db70a/electriccommander-release.apk)

## Notes
- Set your box to silent mode before starting the app and connecting it to a power box
- Play arround a bit just listening to the output to get a feeling for the settings
- The app starts a background service when you start to generate some output. It can be stopped, stopping all output, from the android notification bar.

## Notes for Estim 2b Box
- You should be able to connect the box to the phone using the included stereo audio cable
- The 2b box audio socket does not always fit to other audio plugs
- I had some issues with my phone, either due to Android not recognising the box as head phones properly or due to the plug not fitting properly into my phone
- As an alernative I recommend usinge bluetooth and to attach a bluetooth receiver to the box
- Connecting the "Mpow Streambot Bluetooth Receiver" to the box using the supplied stereo cable worked perfectly for me

## Ideas

- Movement triggered pulses
- Custom sound patterns
- Camera-based movement detection
- Remote control via Webserver
- Remote control via XMPP
- GPS trigger?
- Sound-level based stimulation
- Random shocks

## Technology and Building
The app is written in Scala using Scala.Rx to do most of the event handling and Macroid to describe the UI. It is build using SBT. Install the Android SDK and set "ANDROID_HOME". You should be able to directly build and run it calling "sbt android:run" if you have a phone or simulate properly set up.

## Some screenshots (App also contains english localization)

![menu](/uploads/479e6e5043b4ff2f5cc6623981af89a1/menu.png)

![continuous](/uploads/e28da8662c3900993276a80bf1006f89/continuous.png)

![soundtriggered](/uploads/e850ff92573bb357e288b448524dfcd2/soundtriggered.png)
