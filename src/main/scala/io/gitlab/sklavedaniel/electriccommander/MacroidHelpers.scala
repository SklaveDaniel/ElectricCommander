package io.gitlab.sklavedaniel.electriccommander

import android.content.res.ColorStateList
import android.graphics.drawable.{Drawable, RippleDrawable}
import android.graphics.{Color, PorterDuff, Typeface}
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.util.TypedValue
import android.view.ViewGroup.LayoutParams._
import android.view.{Gravity, View, ViewGroup}
import android.widget.RadioGroup.OnCheckedChangeListener
import android.widget.RelativeLayout._
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget._
import com.fortysevendeg.macroid.extras.SeekBarTweaks._
import com.fortysevendeg.macroid.extras.ViewTweaks._
import macroid.FullDsl._
import macroid._
import macroid.contrib._
import rx.{Ctx, Rx, Var}

import language.postfixOps

object MacroidHelpers {

  def tvAppearance(appearance: Int): Tweak[TextView] = Tweak[TextView](_.setTextAppearance(appearance))

  def vRipple(ripple: Int)(implicit ctx: ContextWrapper): Tweak[ImageView] = Tweak[ImageView] { iv => {
    iv.setBackground(ctx.application.getResources.getDrawable(ripple, null))
  }
  }

  def ivSrcColored(src: Int, color: Int)(implicit ctx: ContextWrapper) = Tweak[ImageView] { iv => {
    val drawable = ctx.application.getResources.getDrawable(R.drawable.ic_drawer, null)
    drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP)
    iv.setImageDrawable(drawable)
    Ui
  }
  }

  def dlToggle() = Tweak[DrawerLayout](dl =>
    if (dl.isDrawerOpen(Gravity.START)) dl.closeDrawer(Gravity.START) else dl.openDrawer(Gravity.START))

  def dlOpen(open: Boolean) = Tweak[DrawerLayout](dl =>
    if (open) dl.closeDrawer(Gravity.START) else dl.openDrawer(Gravity.START))

  def dlIsOpen(drawer: Ui[DrawerLayout]) = drawer.map(_.isDrawerOpen(Gravity.START))

  def UiGet[T](option: Option[T]) = Ui(option.get)

  def nvAddHeader(view: View) = Tweak[NavigationView](_.addHeaderView(view))

  def nvTintList(tintList: Option[ColorStateList]) = Tweak[NavigationView](_.setItemIconTintList(tintList.orNull))

  def nvAddItem(id: Int, title: String, icon: Option[Int]) = Tweak[NavigationView](nv => {
    val item = nv.getMenu.add(0, id, id, title)
    icon.foreach(i => item.setIcon(i))
  })

  def nvItemTextColor(color: ColorStateList) = Tweak[NavigationView](_.setItemTextColor(color))

  def ivAdjBounds() = Tweak[ImageView](_.setAdjustViewBounds(true))

  def llGravity(gravity: Int) = Tweak[LinearLayout](_.setGravity(gravity))

  def vAddView(view: View, pos: Int) = Tweak[DrawerLayout](_.addView(view, pos))

  def vRemoveView(pos: Int) = Tweak[DrawerLayout](_.removeViewAt(pos))

  def vAddViews(views: Ui[Seq[View]]*) = Tweak[ViewGroup] { vg => for (ui <- views; view <- ui.get) vg.addView(view) }

  def nvCheckedItem(id: Int) = Tweak[NavigationView](_.setCheckedItem(id))

  def nvCheckable(checkable: Boolean, exclusive: Boolean) = Tweak[NavigationView](_.getMenu.setGroupCheckable(0, checkable, exclusive))

  def rlRules(w: Int, h: Int, rules: Seq[Int], subjectRules: Seq[(Int, Int)]) = Tweak[View](rl => {
    val params = new RelativeLayout.LayoutParams(w, h)
    for (rule <- rules) params.addRule(rule)
    for ((rule, subject) <- subjectRules) params.addRule(rule, subject)
    rl.setLayoutParams(params)
  })

  def tvMonospace() = Tweak[TextView](tv => tv.setTypeface(Typeface.MONOSPACE))

  def llShowDividers(deviders: Int) = Tweak[LinearLayout](_.setShowDividers(deviders))

  def llDividerDrawable(drawable: Drawable) = Tweak[LinearLayout](_.setDividerDrawable(drawable))

  def llDividerPadding(padding: Int) = Tweak[LinearLayout](_.setDividerPadding(padding))

  def swChecked(checked: Boolean) = Tweak[Switch](_.setChecked(checked))

  def swCheckedChange(listener: CompoundButton.OnCheckedChangeListener) = Tweak[Switch](_.setOnCheckedChangeListener(listener))

  def buildSeekBar(below: Int, align: Option[Int], labelText: Int,
                   minValue: Double, maxValue: Double, initValue: Double, scale: Double, format: Double => String, unitName: String,
                   ids: IdScope)(implicit ctx: ContextWrapper, owner: Ctx.Owner) = {
    object seekBarIds extends ids.SubScope
    val alignCnstr = align.map(id => (ALIGN_LEFT, id) :: (ALIGN_RIGHT, id) :: Nil).getOrElse(Nil)
    var seekBar = slot[SeekBar]
    var unit = slot[TextView]
    val value = Var(initValue)
    (Ui.sequence(
      w[TextView] <~ text(labelText) + tvAppearance(R.style.TextAppearance_AppCompat_Medium)
        + rlRules(WRAP_CONTENT, WRAP_CONTENT, ALIGN_PARENT_LEFT :: Nil, (BELOW, below) :: Nil)
        + vMargin(marginLeft = 8 dp) + id(seekBarIds.label),
      w[TextView] <~ text(format(initValue)) + tvAppearance(R.style.TextAppearance_AppCompat_Medium)
        + rlRules(WRAP_CONTENT, WRAP_CONTENT, Nil, (BELOW, below) :: (LEFT_OF, seekBarIds.unitName)
        :: (ALIGN_BASELINE, seekBarIds.label) :: Nil)
        + id(seekBarIds.unit) + wire(unit) + tvMonospace(),
      w[TextView] <~ text(unitName) + tvAppearance(R.style.TextAppearance_AppCompat_Medium)
        + rlRules(WRAP_CONTENT, WRAP_CONTENT, ALIGN_PARENT_RIGHT :: Nil, (BELOW, below)
        :: (ALIGN_BASELINE, seekBarIds.label) :: Nil)
        + id(seekBarIds.unitName) + vPadding(paddingLeft = 2 dp),
      w[SeekBar] <~ rlRules(WRAP_CONTENT, WRAP_CONTENT, Nil,
        (BELOW, below) :: (RIGHT_OF, seekBarIds.label) :: (LEFT_OF, seekBarIds.unit)
          :: (ALIGN_BOTTOM, seekBarIds.label) :: alignCnstr) + id(seekBarIds.bar) + wire(seekBar)
        + sbMax(((maxValue - minValue) * scale).ceil.toInt) + sbProgress(((initValue - minValue) * scale).round.toInt)
        + sbOnSeekBarChangeListener(new OnSeekBarChangeListener {
        override def onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean): Unit = {
          val v = progress.toDouble / scale + minValue
          (unit <~ text(format(v))).run
          value() = v
        }

        override def onStopTrackingTouch(seekBar: SeekBar): Unit = {}

        override def onStartTrackingTouch(seekBar: SeekBar): Unit = {}
      })
    ), seekBarIds, value)
  }

  def follow[T](inRx: Rx[T], outVar: Var[T])(implicit owner: Ctx.Owner): Unit = {
    inRx.fold(None: Option[T]) {
      case (None, v) =>
        outVar() = v
        Some(v)
      case (Some(v), nv) =>
        if (v != nv) {
          outVar() = nv
        }
        Some(nv)
    }
  }

  def bind[T](var1: Var[T], var2: Var[T])(implicit owner: Ctx.Owner): Unit = {
    follow(var1, var2)
    follow(var2, var1)
  }

  def wave(frequency: Double, volume: Double, priority: Double) =
    (priority, (t: Double) => Math.sin(2 * Math.PI * t * frequency) * volume)
}
