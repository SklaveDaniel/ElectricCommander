package io.gitlab.sklavedaniel.electriccommander

import java.util

import java.util.concurrent.LinkedBlockingDeque

import android.app.{PendingIntent, Service}
import android.content.Intent
import android.media.{AudioFormat, AudioManager, AudioTrack}
import android.os.{Binder, IBinder}
import android.support.v4.app.NotificationCompat
import io.gitlab.sklavedaniel.electriccommander.GeneratorService._

import scala.collection.immutable.SortedMap
import rx._

object GeneratorService {

  sealed abstract class Output

  case object Left extends Output

  case object Right extends Output

  sealed abstract class GeneratorAction

  case class PlayAction(id: String, wave: Option[(Double, Double => Double)], output: Output) extends GeneratorAction

  case object StopAction extends GeneratorAction

  case class AudioBinder(service: GeneratorService) extends Binder

  trait GeneratorListener {
    def stopped(): Unit
    def started(): Unit
  }

  val STOP_ACTION_ID = "stop"

}

class GeneratorService extends Service {

  import Ctx.Owner.Unsafe._

  private var audioThread: Option[Thread] = None

  private val actionQueue: util.Queue[GeneratorAction] = new LinkedBlockingDeque[GeneratorAction]()

  val playing = Var(false)

  override def onStartCommand(intent: Intent, flags: Int, startId: Int): Int = {
    if(intent.getAction == STOP_ACTION_ID) {
      execute(StopAction)
    }
    Service.START_STICKY
  }

  def execute(action: GeneratorAction): Unit = {
    action match {
      case PlayAction(_, _, _) =>
        if (audioThread.isEmpty) {

          val notificationIntent = new Intent(this, classOf[MainActivity])
          notificationIntent.setAction(Intent.ACTION_MAIN)
          notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER)
          notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
          val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

          val stopIntent = new Intent(this, classOf[GeneratorService])
          stopIntent.setAction(STOP_ACTION_ID)
          val pendingStopIntent = PendingIntent.getService(this, 0, stopIntent, 0)

          val notification = new NotificationCompat.Builder(this)
            .setContentTitle(getResources.getString(R.string.app_name)).setContentIntent(pendingIntent).setContentText(getResources.getString(R.string.notification_msg))
            .setSmallIcon(R.drawable.notification_icon)
            .setOngoing(true).addAction(R.drawable.stop, getResources.getString(R.string.stop_notification), pendingStopIntent). build()
          startForeground(1, notification)
          val thread = new Thread() {

            val sampleRate = 20000
            var leftWaves: SortedMap[(Double, String), Double => Double] = SortedMap((Double.PositiveInfinity, "default") -> ((t: Double) => 0.0))
            var leftPriorities: Map[Any, Double] = Map("default" -> Double.PositiveInfinity)
            var rightWaves: SortedMap[(Double, String), Double => Double] = SortedMap((Double.PositiveInfinity, "default") -> ((t: Double) => 0.0))
            var rightPriorities: Map[Any, Double] = Map("default" -> Double.PositiveInfinity)
            var leftWave: Double => Double = leftWaves.head._2
            var rightWave: Double => Double = rightWaves.head._2

            def genTone(sample: Array[Float], offset: Int): Unit = {
              for (i <- 0 until sample.length / 2) {
                sample(2 * i) = leftWave(i.toDouble / sampleRate).toFloat
                sample(2 * i + 1) = rightWave(i.toDouble / sampleRate).toFloat
              }
            }

            def updateMaps(waves: SortedMap[(Double, String), Double => Double], priorities: Map[Any, Double], id: String, wave: Option[(Double, Double => Double)]) = {
              val lastPriority = priorities.get(id)
              val newPriorities = priorities -- lastPriority
              val newWaves = waves -- lastPriority.map(p => (p, id))
              wave match {
                case Some((p, w)) =>
                  (newWaves + ((p, id) -> w), newPriorities + (id -> p))
                case None =>
                  (newWaves, newPriorities)
              }
            }

            override def run(): Unit = {
              val audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_OUT_STEREO,
                AudioFormat.ENCODING_PCM_FLOAT, sampleRate,
                AudioTrack.MODE_STREAM)
              audioTrack.play()
              val data = new Array[Float](200)
              var offset = 0
              var play = true
              while (play) {
                for (action <- Stream.continually(actionQueue.poll()).takeWhile(_ != null)) {
                  action match {
                    case StopAction =>
                      play = false
                    case PlayAction(id, wave, output) =>
                      output match {
                        case Left =>
                          val temp = updateMaps(leftWaves, leftPriorities, id, wave)
                          leftWaves = temp._1
                          leftPriorities = temp._2
                          leftWave = leftWaves.head._2
                        case Right =>
                          val temp = updateMaps(rightWaves, rightPriorities, id, wave)
                          rightWaves = temp._1
                          rightPriorities = temp._2
                          rightWave = rightWaves.head._2
                      }
                  }
                }
                genTone(data, offset)
                offset += data.length / 2
                audioTrack.write(data, 0, data.length, AudioTrack.WRITE_BLOCKING)
              }

              audioTrack.release()
            }

          }
          audioThread = Some(thread)
          thread.start()
          playing() = true
        }
        actionQueue.offer(action)
      case StopAction if audioThread.isDefined =>
        actionQueue.offer(action)
        audioThread = None
        playing() = false
        stopForeground(true)
        stopSelf()
      case _ =>
    }
  }

  override def onBind(intent: Intent): IBinder = {
    AudioBinder(this)
  }

  override def onDestroy(): Unit = {
    execute(StopAction)
  }

}