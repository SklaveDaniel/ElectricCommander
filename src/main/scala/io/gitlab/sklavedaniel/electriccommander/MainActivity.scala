package io.gitlab.sklavedaniel.electriccommander

import language.postfixOps
import android.app.Activity
import android.os.{Bundle, IBinder}
import android.widget._
import macroid._
import macroid.contrib._
import macroid.FullDsl._
import com.fortysevendeg.macroid.extras.ViewTweaks._
import com.fortysevendeg.macroid.extras.TextTweaks._
import android.content.res.ColorStateList
import android.graphics.Color
import android.support.design.widget.NavigationView
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.AppCompatActivity
import android.view.{Gravity, MenuItem, View, ViewGroup}
import com.fortysevendeg.macroid.extras.ImageViewTweaks._
import MacroidHelpers._
import android.content.{ComponentName, Context, Intent, ServiceConnection}
import io.gitlab.sklavedaniel.electriccommander.GeneratorService.{AudioBinder, GeneratorListener}
import rx._

class MainActivity extends AppCompatActivity with Contexts[Activity] {
  self =>

  import Ctx.Owner.Unsafe._

  val audioService = Var(None: Option[GeneratorService])

  val active = Var(false)

  var selectedView = Var(None: Option[MainContentView])

  private var views: Option[Seq[MainContentView]] = None

  private lazy val connection: ServiceConnection = new ServiceConnection {

    override def onServiceDisconnected(name: ComponentName): Unit = {
      audioService() = None
    }

    override def onServiceConnected(name: ComponentName, service: IBinder): Unit = {
      val as = service.asInstanceOf[AudioBinder].service
      audioService() = Some(as)
    }
  }

  override def onCreate(savedInstanceState: Bundle) = {
    super.onCreate(savedInstanceState)

    bindService(new Intent(self, classOf[GeneratorService]), connection, Context.BIND_AUTO_CREATE)

    implicit object Ids extends RootIdScope

    var drawer = slot[DrawerLayout]
    var navigation = slot[NavigationView]
    var title = slot[TextView]

    val tmpViews = IndexedSeq(
      new ContinuousContentView(self),
      new SoundtriggeredContentView(self)
    )
    views = Some(tmpViews)
    selectedView() = Some(tmpViews(0))

    val items = (for ((view, id) <- tmpViews.zipWithIndex) yield nvAddItem(id, view.title, view.icon)).reduce(_ + _)

    val customRed = 0xff6D0619
    val colorStateList = new ColorStateList(
      Array(Array(android.R.attr.state_checked), Array()), Array(customRed, Color.BLACK))

    setContentView {
      Ui.get {
        l[LinearLayout](
          l[LinearLayout](
            w[ImageView] <~ vRipple(R.drawable.action_bar_ripple) + ivSrcColored(R.drawable.ic_drawer, Color.WHITE) + vPaddings(2 dp)
              <~ On.click {
              drawer <~ dlToggle()
            },
            w[TextView] <~ text(tmpViews(0).title)
              + tvGravity(Gravity.CENTER)
              + lp[LinearLayout](ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.CENTER)
              + tvAppearance(R.style.Base_TextAppearance_AppCompat_Widget_ActionBar_Title) + wire(title)

          ) <~ horizontal + vMatchWidth + vBackgroundColor(R.color.background_material_light) + vPadding(3 dp, 3 dp, 13 dp, 7 dp)
            + vBackground(R.drawable.actionbar_border) + llGravity(Gravity.CENTER),
          l[DrawerLayout](
            w[NavigationViewFix] <~ lp[DrawerLayout](280 dp, ViewGroup.LayoutParams.MATCH_PARENT, Gravity.START)
              + vBackgroundColor(0xffd0d0d0) + nvItemTextColor(colorStateList) + nvAddHeader(Ui.get {
              l[LinearLayout](
                w[ImageView] <~ ivSrc(R.drawable.app_logo) + lp[LinearLayout](ViewGroup.LayoutParams.WRAP_CONTENT, 80 dp) + ivAdjBounds(),
                w[TextView] <~ text(R.string.app_name) + tvAppearance(R.style.TextAppearance_AppCompat_Headline) + vPadding(15 dp, 0, 0, 0)
              ) <~ horizontal + vBackgroundColor(customRed) + vContentSizeMatchWidth(100 dp) + vPaddings(5 dp) + llGravity(Gravity.CENTER)
            }) + items + nvTintList(Some(colorStateList)) + nvCheckable(true, true) + nvCheckedItem(0) + wire(navigation)
              + FuncOn.navigationItemSelected[NavigationViewFix]((menuItem: MenuItem) =>
              (navigation <~ nvCheckedItem(menuItem.getItemId))
                ~ (drawer <~ dlToggle())
                ~ (title <~ text(tmpViews(menuItem.getItemId).title))
                ~ (drawer <~ vRemoveView(0))
                ~ (drawer <~ vAddView(tmpViews(menuItem.getItemId).view, 0))
                ~ Ui(selectedView() = Some(tmpViews(menuItem.getItemId)))
                ~ Ui(true))
          ) <~ wire(drawer) <~ vMatchParent + vAddView(tmpViews(0).view, 0)
        ) <~ vertical + vMatchParent
      }
    }

  }

  override def onPause(): Unit = {
    super.onPause()
    active() = false
  }

  override def onResume(): Unit = {
    super.onResume()
    active() = true
  }

  override def onRequestPermissionsResult(requestCode: Int, permissions: Array[String], grantResults: Array[Int]): Unit = {
    views.foreach(_.foreach(_.onRequestPermissionsResult(requestCode, permissions, grantResults)))
  }

  override def onDestroy(): Unit = {
    super.onDestroy()
    unbindService(connection)
  }

}