package io.gitlab.sklavedaniel.electriccommander

import language.dynamics

abstract class IdScope(baseGetId: Any => Int) extends Dynamic {

  protected def getId(key: Any): Int = baseGetId(key)

  def selectDynamic(tag: String) = getId(tag)

  class SubScope extends IdScope(baseGetId) {
    override protected def getId(key: Any): Int = super.getId((this, key))
  }

}

class IdGenerator {
  private var ids = Map.empty[Any, Int]
  private var counter = 1

  private val lock = new Object

  val baseGetId = (key: Any) => lock synchronized {
    ids.getOrElse(key, {
      counter += 1
      ids += key -> counter
      counter
    })
  }
}

class RootIdScope extends IdScope(new IdGenerator().baseGetId)
