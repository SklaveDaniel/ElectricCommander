package io.gitlab.sklavedaniel.electriccommander

import language.postfixOps
import android.view.View
import macroid._
import macroid.contrib._
import macroid.FullDsl._
import com.fortysevendeg.macroid.extras.ImageViewTweaks._
import MacroidHelpers._
import com.fortysevendeg.macroid.extras.ViewTweaks._
import com.fortysevendeg.macroid.extras.TextTweaks._
import com.fortysevendeg.macroid.extras.SeekBarTweaks.{W, _}
import android.widget._
import android.view.ViewGroup.LayoutParams.{MATCH_PARENT, WRAP_CONTENT}
import RelativeLayout._
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.SeekBar.OnSeekBarChangeListener
import rx._

class ContinuousContentView(main: MainActivity)(implicit ctx: ContextWrapper, private val ids: IdScope, owner: Ctx.Owner) extends MainContentView {

  private object LocalIds extends ids.SubScope

  override def icon: Option[Int] = None

  override def title: String = ctx.application.getString(R.string.activity_continuous_name)

  private class MyOutputSettings(title: Int, output: GeneratorService.Output)
    extends OutputSettings(title, output, LocalIds, main, "ContinuousStimulation") {

    def play(): Unit = {
      main.audioService.now.foreach(as => {
        if (playing.now) {
          as.execute(GeneratorService.PlayAction(audioId,
            Some(wave(frequency.now, volume.now, priority.now)), output))
        }
      })
    }

    frequency.trigger(play())
    volume.trigger(play())
    priority.trigger(play())
    playing.trigger(play())

  }

  private val left = new MyOutputSettings(R.string.gui_left, GeneratorService.Left)
  private val right = new MyOutputSettings(R.string.gui_right, GeneratorService.Right)

  override val view: View = Ui.get {
    l[LinearLayout](
      left.view,
      right.view
    ) <~ vMatchParent + vertical + llShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE) +
      llDividerDrawable(ctx.application.getDrawable(R.drawable.divider)) +
      llDividerPadding(15 dp)
  }

}
