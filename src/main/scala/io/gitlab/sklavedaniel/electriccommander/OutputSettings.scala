package io.gitlab.sklavedaniel.electriccommander

import android.view.ViewGroup.LayoutParams._
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.RelativeLayout._
import android.widget.{CompoundButton, RelativeLayout, Switch, TextView}
import com.fortysevendeg.macroid.extras.ViewTweaks._
import macroid.FullDsl._
import rx._
import MacroidHelpers._
import android.view.View
import macroid.{ContextWrapper, Ui}

import language.postfixOps

abstract class OutputSettings(title: Int, val output: GeneratorService.Output, val ids: IdScope, main: MainActivity, val audioId: String, initPriority: Int = 0)
                    (implicit ctx: ContextWrapper, owner: Ctx.Owner) {

  object OutputIds extends ids.SubScope

  val (frequencyBar, frequencyIds, frequency) = buildSeekBar(OutputIds.heading, None, R.string.gui_frequency, 100, 6000, 500, 1, v => f"${v.round}%4d", "Hz", OutputIds)
  val (volumeBar, volumeIds, volume) = buildSeekBar(frequencyIds.label, Some(frequencyIds.bar), R.string.gui_volume, 0, 1, 0.5, 10000, v => f"${(v * 100).round}%3d", "%", OutputIds)
  val (priorityBar, priorityIds, priority) = buildSeekBar(volumeIds.label, Some(frequencyIds.bar), R.string.gui_priority, -999, 999, initPriority, 1, v => f"${v.round}%3d", "", OutputIds)

  val playing = Var(false)

  var switch = slot[Switch]

  def additionalViews: Seq[Ui[Seq[View]]] = Seq()

  lazy val view = l[RelativeLayout](
    w[TextView] <~ text(title) + tvAppearance(R.style.TextAppearance_AppCompat_Medium)
      + rlRules(WRAP_CONTENT, WRAP_CONTENT, ALIGN_PARENT_LEFT :: ALIGN_PARENT_TOP :: Nil, Nil)
      + id(OutputIds.heading) + vMargin(marginBottom = 5 dp),
    w[Switch] <~ tvAppearance(R.style.TextAppearance_AppCompat_Medium)
      + rlRules(WRAP_CONTENT, WRAP_CONTENT, ALIGN_PARENT_RIGHT :: ALIGN_PARENT_TOP :: Nil, Nil)
      + swChecked(playing.now) + swCheckedChange(new OnCheckedChangeListener {
      override def onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean): Unit = {
        playing() = isChecked
      }
    }) + wire(switch)
  ) <~ vAddViews(frequencyBar, volumeBar, priorityBar) + vAddViews(additionalViews: _*) +
    vMatchWidth + vPaddings(8 dp)

  playing.triggerLater(switch.get.setChecked(playing.now))
  playing.foreach(b => if (!b) {
    main.audioService.now.foreach(as => if (as.playing.now) {
      as.execute(GeneratorService.PlayAction(audioId, None, output))
    })
  })
  main.audioService.foreach(_.foreach(_.playing.foreach(b => if (!b) {
    playing() = false
  })))

}

