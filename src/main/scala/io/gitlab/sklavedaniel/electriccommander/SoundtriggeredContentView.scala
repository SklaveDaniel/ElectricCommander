package io.gitlab.sklavedaniel.electriccommander

import language.postfixOps
import android.view.View
import macroid._
import macroid.contrib._
import macroid.FullDsl._
import com.fortysevendeg.macroid.extras.ImageViewTweaks._
import MacroidHelpers._
import com.fortysevendeg.macroid.extras.ViewTweaks.{W, _}
import com.fortysevendeg.macroid.extras.TextTweaks._
import com.fortysevendeg.macroid.extras.SeekBarTweaks.{W, _}
import android.widget._
import android.view.ViewGroup.LayoutParams.{MATCH_PARENT, WRAP_CONTENT}
import RelativeLayout._
import android.content.pm.PackageManager
import android.media.{AudioFormat, AudioRecord}
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.CompoundButton.OnCheckedChangeListener
import android.widget.SeekBar.OnSeekBarChangeListener
import rx._

import scala.collection.immutable.Queue
import scala.collection.mutable

class SoundtriggeredContentView(main: MainActivity)(implicit ctx: ContextWrapper, private val ids: IdScope, owner: Ctx.Owner) extends MainContentView {

  val sampleRate = 16000
  val displayTime = 10.0
  val displayStep = 0.033

  private object LocalIds extends ids.SubScope

  override def title: String = ctx.application.getString(R.string.activity_soundtriggered_name)

  override def icon: Option[Int] = None

  var waveView = slot[WaveView]

  val audioPermission = Var(ContextCompat.checkSelfPermission(main, android.Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED)
  val active = Rx {
    main.active() && main.selectedView().map(_ == this).getOrElse(false)
  }
  val running = Rx {
    audioPermission() && (active() || main.audioService().map(_.playing()).getOrElse(false))
  }
  active.foreach(b => if (b && !audioPermission.now) {
    ActivityCompat.requestPermissions(main, Array(android.Manifest.permission.RECORD_AUDIO), 1)
  })
  val thread = running.fold(None: Option[DetectionThread]) {
    case (None, true) =>
      val t = new DetectionThread(Seq(
        left.callback,
        right.callback
      ))
      t.start()
      Some(t)
    case (Some(t), false) =>
      t.record = false
      None
    case (t, _) => t
  }

  private[SoundtriggeredContentView] val (smoothingBar, smoothingIds, smoothing) = buildSeekBar(LocalIds.wave, None, R.string.gui_smoothing, 0.033, 1, 0.1, 100, v => f"${v}%1.2f", "s", LocalIds)
  private[SoundtriggeredContentView] val (limitBar, limitIds, limit) = buildSeekBar(smoothingIds.label, Some(smoothingIds.bar), R.string.gui_limit, 0, 1, 0.5, 1000, v => f"${(v * 100).round}%3d", "%", LocalIds)
  private[SoundtriggeredContentView] val (zoomBar, zoomIds, zoom) = buildSeekBar(limitIds.label, Some(smoothingIds.bar), R.string.gui_zoom, 1, 50, 5, 100, v => f"${v.round}%2d", "x", LocalIds)

  private class MyOutputSettings(title: Int, output: GeneratorService.Output) extends OutputSettings(title, output, LocalIds, main, "Soundtriggered", -10) {

    private[SoundtriggeredContentView] val (durationBar, durationIds, duration) = buildSeekBar(priorityIds.label, Some(priorityIds.bar), R.string.gui_duration, 1, 60, 5, 240, v => f"${v.round}%2d", "s", OutputIds)
    private[SoundtriggeredContentView] val (pauseBar, pauseIds, pause) = buildSeekBar(durationIds.label, Some(priorityIds.bar), R.string.gui_pause, 0, 60, 10, 240, v => f"${v.round}%2d", "s", OutputIds)

    override def additionalViews: Seq[Ui[Seq[View]]] = {
      Seq(
        durationBar, pauseBar
      )
    }

    val shocking = Var(false)

    val shockingActive = Rx {
      shocking() && playing()
    }

    shockingActive.foreach(b => if (b) {
      main.audioService.now.foreach(as => {
        as.execute(GeneratorService.PlayAction(audioId,
          Some(wave(frequency.now, volume.now, priority.now)), output))
      })
    } else {
      main.audioService.now.foreach(as => if (as.playing.now) {
        as.execute(GeneratorService.PlayAction(audioId,
          None, output))
      })
    })

    val shockingInactive = Rx {
      !shocking() && playing()
    }

    shockingInactive.foreach(b => if (b) {
      main.audioService.now.foreach(_.execute(GeneratorService.PlayAction(audioId,
        None, output))
      )
    })


    private var timeout = 0
    private var shockTimeout = 0

    val callback = (avg: Double, sampleRate: Int) => {
      if (timeout >= 0) {
        timeout -= 1
      } else if (avg > absoluteLimit.now) {
        timeout = (duration.now + pause.now).toInt * sampleRate
        shockTimeout = duration.now.toInt * sampleRate
        shocking() = true
      }
      if (shockTimeout >= 0) {
        shockTimeout -= 1
      } else if (shocking.now) {
        shocking() = false
      }
    }
  }

  private[SoundtriggeredContentView] val left = new MyOutputSettings(R.string.gui_left, GeneratorService.Left)
  private[SoundtriggeredContentView] val right = new MyOutputSettings(R.string.gui_right, GeneratorService.Right)

  override val view: View = Ui.get {
    l[ScrollView](
      l[LinearLayout](
        l[RelativeLayout] {
          w[WaveView] <~ vContentSizeMatchWidth(150 dp) + wire(waveView) + id(LocalIds.wave) + vPadding(paddingBottom = 8 dp)
        } <~ vMatchWidth + vAddViews(smoothingBar, limitBar, zoomBar) + vPaddings(8 dp),
        left.view,
        right.view
      ) <~ vMatchParent + vertical + llShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE) +
        llDividerDrawable(ctx.application.getDrawable(R.drawable.divider)) +
        llDividerPadding(15 dp)
    ) <~ vMatchWidth
  }

  waveView.get.points() = Queue.fill((displayTime / displayStep).toInt)((displayStep / displayTime, 0.0))

  limit.foreach(l => waveView.get.limit() = Some(l))

  val absoluteLimit = Rx {
    limit() / zoom()
  }

  override def onRequestPermissionsResult(requestCode: Int, permissions: Array[String], grantResults: Array[Int]): Unit = {
    requestCode match {
      case 1 =>
        if (grantResults.length > 0 && grantResults(0) == PackageManager.PERMISSION_GRANTED) {
          audioPermission() = true
        }
    }
  }

  class DetectionThread(callbacks: Seq[(Double, Int) => Unit]) extends Thread {
    @volatile
    var record = true

    override def run(): Unit = {

      val bufferSize = AudioRecord.getMinBufferSize(sampleRate, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_FLOAT)
      val recorder = new AudioRecord(1, //MIC
        sampleRate,
        AudioFormat.CHANNEL_IN_MONO,
        AudioFormat.ENCODING_PCM_FLOAT,
        bufferSize)
      val buffer = new Array[Float](bufferSize)
      recorder.startRecording()

      val queue = mutable.Queue[Double]()
      var sum = 0.0
      var displayTimeout = 0.0

      while (record) {
        recorder.read(buffer, 0, bufferSize, AudioRecord.READ_BLOCKING)
        for (v <- buffer) {
          sum += v.abs
          queue.enqueue(v.abs)
          val timeWindow = smoothing.now
          while (queue.length > timeWindow * sampleRate) {
            sum -= queue.dequeue()
          }

          val avg = sum / timeWindow / sampleRate
          for (callback <- callbacks) callback(avg, sampleRate)

          displayTimeout += 1.0 / sampleRate
          if (displayTimeout > displayStep) {
            displayTimeout = 0.0
            if (active.now) {
              val points = waveView.get.points.now.enqueue((displayStep / displayTime, (avg * zoom.now).min(1.0)))
              waveView.get.points() = if (points.length * displayStep > displayTime) {
                points.dequeue._2
              } else points
            }
          }
        }
      }
      recorder.release()
    }
  }


}
