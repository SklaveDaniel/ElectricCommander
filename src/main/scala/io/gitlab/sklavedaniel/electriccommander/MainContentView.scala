package io.gitlab.sklavedaniel.electriccommander

import android.view.View

trait MainContentView {
  def view: View

  def title: String

  def icon: Option[Int]

  def onRequestPermissionsResult(requestCode: Int, permissions: Array[String], grantResults: Array[Int]): Unit = {}
}