package io.gitlab.sklavedaniel.electriccommander

import language.postfixOps
import android.content.Context
import android.graphics.{Canvas, Color, Paint}
import android.util.AttributeSet
import android.view.View
import macroid.MediaQueries._
import macroid.Ui
import rx._

import scala.collection.immutable.Queue

class WaveView(context: Context)(implicit owner: Ctx.Owner) extends View(context, null) {


  val background = new Paint(Paint.ANTI_ALIAS_FLAG)
  background.setColor(Color.WHITE)
  background.setStyle(Paint.Style.FILL)

  val foreground = new Paint(Paint.ANTI_ALIAS_FLAG)
  foreground.setColor(Color.RED)
  foreground.setStrokeWidth(3)
  foreground.setStyle(Paint.Style.STROKE)

  val points = Var(Queue[(Double, Double)]())
  points.trigger(Ui(invalidate()).run)
  val limit = Var(None: Option[Double])
  limit.trigger(Ui(invalidate()).run)

  override def onDraw(canvas: Canvas): Unit = {
    val left = getPaddingLeft
    val top = getPaddingTop
    val right = getWidth - getPaddingRight
    val bottom = getHeight - getPaddingBottom
    val width = getWidth - getPaddingLeft - getPaddingRight
    val height = getHeight - getPaddingTop - getPaddingBottom
    canvas.drawRect(left, top, right, bottom, background)
    val array = points.now.scanLeft((0.0f, Seq[Float]())) {
      case ((s, _), (x, y)) => (s + x.toFloat, Seq((left + (s + x) * width).toFloat, (top + (1.0 - y) * height).toFloat))
    }.tail.map(_._2).sliding(2).flatten.flatten.toArray
    canvas.drawLines(array, foreground)
    limit.now.foreach(l => {
      val y = top + (1.0 - l).toFloat * height
      canvas.drawLine(left, y, right, y, foreground)
    })
    canvas.drawRect(left, top, right, bottom, foreground)
  }
}
