package io.gitlab.sklavedaniel.electriccommander

import android.content.Context
import android.support.design.widget.NavigationView
import android.support.design.widget.NavigationView.OnNavigationItemSelectedListener

class NavigationViewFix(context: Context) extends NavigationView(context) {

  def setOnNavigationItemSelectedListener(listener: OnNavigationItemSelectedListener): Unit =
    setNavigationItemSelectedListener(listener)
}
